# Open Flights Demo

Open Flights demo app to show how resilience engineering, observability and chaos engineering  play together.

The demo is built using .NET Core 2.2, and has an API and a crawler to measure the response times.

To play with it you need to configure the settings on `appsettings.json`. You can use the [app secrets](https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-2.2&tabs=windows).