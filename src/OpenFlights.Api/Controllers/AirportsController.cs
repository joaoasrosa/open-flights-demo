using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using OpenFlights.Api.Persistence;

namespace OpenFlights.Api.Controllers
{
    [Route("api/airports")]
    [ApiController]
    public class AirportsController : ControllerBase
    {
        private readonly IAirportRepository _airportRepository;
        private readonly IRouteRepository _routeRepository;
        private readonly IAirlineRepository _airlineRepository;

        public AirportsController(
            IAirportRepository airportRepository,
            IRouteRepository routeRepository,
            IAirlineRepository airlineRepository
        )
        {
            _airportRepository = airportRepository ??
                                 throw new ArgumentNullException(nameof(airportRepository));
            _routeRepository = routeRepository ??
                               throw new ArgumentNullException(nameof(routeRepository));
            _airlineRepository = airlineRepository ??
                                 throw new ArgumentNullException(nameof(airlineRepository));
        }

        /// <summary>
        /// Returns the list of airports.
        /// </summary>
        /// <returns>The airports list</returns>
        /// <response code="200">Returns the airport list</response>
        /// <response code="500">An internal server error occured</response>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetAirports()
        {
            try
            {
                var airports = await _airportRepository.FetchAirports();
                return new OkObjectResult(airports);
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }

        /// <summary>
        /// Returns an airport by its id.
        /// </summary>
        /// <returns>The airport</returns>
        /// <response code="200">Returns the airport</response>
        /// <response code="204">Returns empty if no airport was found</response>
        /// <response code="500">An internal server error occured</response>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetAirport([FromRoute] uint id)
        {
            try
            {
                var airport = await _airportRepository.FetchAirport(id);

                if (airport != null)
                    return new OkObjectResult(airport);

                return new NoContentResult();
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }

        /// <summary>
        /// Returns the routes of an airport by its id.
        /// </summary>
        /// <returns>The airport routes</returns>
        /// <response code="200">Returns the airport routes</response>
        /// <response code="204">Returns empty if no airport was found</response>
        /// <response code="500">An internal server error occured</response>
        [HttpGet("{id}/routes")]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetAirportRoutes([FromRoute] uint id)
        {
            try
            {
                var airportRoutes = await _routeRepository.FetchAirportRoutes(id);

                if (airportRoutes != null&& airportRoutes.Any())
                    return new OkObjectResult(airportRoutes);

                return new NoContentResult();
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }

        /// <summary>
        /// Returns the airlines that operate at an airport by its id.
        /// </summary>
        /// <returns>The airlines that operate in the airport</returns>
        /// <response code="200">Returns the airlines that operate in an airport</response>
        /// <response code="204">Returns empty if no airport was found</response>
        /// <response code="500">An internal server error occured</response>
        [HttpGet("{id}/airlines")]
        [ProducesResponseType(200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetAirportAirlines([FromRoute] uint id)
        {
            try
            {
                var airportAirlines = await _airlineRepository.FetchAirportAirlines(id);

                if (airportAirlines != null && airportAirlines.Any())
                    return new OkObjectResult(airportAirlines);

                return new NoContentResult();
            }
            catch
            {
                return new StatusCodeResult(500);
            }
        }
    }
}
