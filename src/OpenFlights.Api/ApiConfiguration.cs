namespace OpenFlights.Api
{
    public class ApiConfiguration
    {
        public DatabaseConfiguration Database { get; set; }

        public OpenWeatherConfiguration OpenWeather { get; set; }
    }

    public class DatabaseConfiguration
    {
        public string Server { get; set; }
        
        public string Database { get; set; }
        
        public string User { get; set; }
        
        public string Password { get; set; }
    }

    public class OpenWeatherConfiguration
    {
        public string Url { get; set; }
        public string ApiKey { get; set; }
    }
}
