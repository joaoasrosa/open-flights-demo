namespace OpenFlights.Api.Models
{
    public class Route
    {
        public uint Id { get; set; }

        public Airport Source { get; set; }

        public Airport Destination { get; set; }

        public Airline Airline { get; set; }
    }
}
