namespace OpenFlights.Api.Models
{
    public class Airline
    {
        public uint Id { get; set; }

        public string Name { get; set; }
    }
}
