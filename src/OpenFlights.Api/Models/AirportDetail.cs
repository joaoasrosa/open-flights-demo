namespace OpenFlights.Api.Models
{
    public class AirportDetail : Airport
    {
        public string Iata { get; set; }
        
        public string Icao { get; set; }
        
        public double Latitude { get; set; }
        
        public double Longitude { get; set; }
        
        public int Elevation { get; set; }
        
        public float UtcOffset { get; set; }
        
        public string DaylightSavingsTime { get; set; }

        public string Timezone { get; set; }

        public Weather Weather { get; set; }
    }
}
