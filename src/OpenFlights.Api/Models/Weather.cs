namespace OpenFlights.Api.Models
{
    public class Weather
    {
        public float Temperature { get; set; }

        public string Conditions { get; set; }
    }
}
