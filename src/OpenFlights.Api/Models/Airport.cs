namespace OpenFlights.Api.Models
{
    public class Airport
    {
        public uint Id { get; set; }
        
        public string Name { get; set; }

        public string City { get; set; }

        public string Country { get; set; }
    }
}
