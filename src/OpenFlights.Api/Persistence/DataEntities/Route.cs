namespace OpenFlights.Api.Persistence.DataEntities
{
    public class Route
    {
        public uint Id { get; set; }
        
        public uint DestinationId { get; set; }
        
        public string DestinationName { get; set; }
        
        public string DestinationCity { get; set; }
        
        public string DestinationCountry { get; set; }
        
        public uint SourceId { get; set; }
        
        public string SourceName { get; set; }
        
        public string SourceCity { get; set; }
        
        public string SourceCountry { get; set; }

        public uint AirlineId { get; set; }

        public string AirlineName { get; set; }
    }
}
