using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using OpenFlights.Api.Models;

namespace OpenFlights.Api.Persistence
{
    public class AirportRepository : IAirportRepository
    {
        private readonly IConnectionFactory _connectionFactory;
        private readonly IOptions<ApiConfiguration> _apiConfiguration;

        public AirportRepository(
            IConnectionFactory connectionFactory,
            IOptions<ApiConfiguration> apiConfiguration
        )
        {
            _connectionFactory = connectionFactory ??
                                 throw new ArgumentNullException(nameof(connectionFactory));
            _apiConfiguration = apiConfiguration ??
                                throw new ArgumentNullException(nameof(apiConfiguration));
        }

        public async Task<IReadOnlyList<Airport>> FetchAirports()
        {
            using (var connection = _connectionFactory.Create())
            {
                var result = (await connection.QueryAsync<Airport>("SELECT apid AS Id,name,city,country FROM airports"))
                    .ToArray();
                return result;
            }
        }

        public async Task<AirportDetail> FetchAirport(uint id)
        {
            using (var connection = _connectionFactory.Create())
            {
                var airport = await connection.QuerySingleOrDefaultAsync<AirportDetail>(
                    "SELECT apid AS Id,name,city,country,iata,icao,x AS latitude,y AS longitude,elevation,timezone AS utcoffset,dst AS daylightsavingstime,tz_id AS timezone FROM airports WHERE apid=@id",
                    new {id});

                airport.Weather = await FetchAirportWeather(airport.City);

                return airport;
            }
        }

        private async Task<Weather> FetchAirportWeather(string city)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_apiConfiguration.Value.OpenWeather.Url);
                var response =
                    await client.GetAsync(
                        $"/data/2.5/weather?q={city}&appid={_apiConfiguration.Value.OpenWeather.ApiKey}&units=metric");
                response.EnsureSuccessStatusCode();

                var stringResult = await response.Content.ReadAsStringAsync();
                var openWeatherResponse = JsonConvert.DeserializeObject<OpenWeatherResponse>(stringResult);

                return openWeatherResponse.ToWeather();
            }
        }
    }
}
