using System.Collections.Generic;
using System.Threading.Tasks;
using OpenFlights.Api.Models;

namespace OpenFlights.Api.Persistence
{
    public interface IAirlineRepository
    {
        Task<IReadOnlyList<Airline>> FetchAirportAirlines(uint id);
    }
}
