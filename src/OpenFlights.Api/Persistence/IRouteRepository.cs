using System.Collections.Generic;
using System.Threading.Tasks;
using OpenFlights.Api.Models;

namespace OpenFlights.Api.Persistence
{
    public interface IRouteRepository
    {
        Task<IReadOnlyList<Route>> FetchAirportRoutes(uint id);
    }
}
