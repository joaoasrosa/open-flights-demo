using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using OpenFlights.Api.Models;

namespace OpenFlights.Api.Persistence
{
    public class RouteRepository : IRouteRepository
    {
        private readonly IConnectionFactory _connectionFactory;

        public RouteRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory ?? throw new ArgumentNullException(nameof(connectionFactory));
        }

        public async Task<IReadOnlyList<Route>> FetchAirportRoutes(uint id)
        {
            using (var connection = _connectionFactory.Create())
            {
                var results = (await connection.QueryAsync<DataEntities.Route>(
                    "SELECT r.rid AS Id," +
                    "d.apid AS destinationid," +
                    "d.name AS destinationname," +
                    "d.city AS destinationcity," +
                    "d.country AS destinationcountry," +
                    "s.apid AS sourceid," +
                    "s.name AS sourcename," +
                    "s.city AS sourcecity," +
                    "s.country AS sourcecountry, " +
                    "a.alid AS airlineid, " +
                    "a.name AS airlinename " +
                    "FROM routes r " +
                    "INNER JOIN airports s ON r.src_apid=s.apid " +
                    "INNER JOIN airports d ON r.dst_apid=d.apid " +
                    "INNER JOIN airlines a ON r.alid=a.alid " +
                    "WHERE s.apid=@id",
                    new {id}
                )).ToArray();

                return results.Select(
                        result => new Route
                        {
                            Id = result.Id,
                            Destination = new Airport
                            {
                                Id = result.DestinationId,
                                Name = result.DestinationName,
                                City = result.DestinationCity,
                                Country = result.DestinationCountry,
                            },
                            Source = new Airport
                            {
                                Id = result.SourceId,
                                Name = result.SourceName,
                                City = result.SourceCity,
                                Country = result.SourceCountry,
                            },
                            Airline = new Airline
                            {
                                Id = result.AirlineId,
                                Name = result.AirlineName
                            }
                        })
                    .ToList();
            }
        }
    }
}
