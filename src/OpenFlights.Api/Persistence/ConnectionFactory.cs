using System;
using System.Data;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;

namespace OpenFlights.Api.Persistence
{
    public class ConnectionFactory : IConnectionFactory
    {
        private readonly DatabaseConfiguration _database;

        public ConnectionFactory(IOptions<ApiConfiguration> apiConfiguration)
        {
            if (apiConfiguration == null)
            {
                throw new ArgumentNullException(nameof(apiConfiguration));
            }

            _database = apiConfiguration.Value.Database;
        }

        public IDbConnection Create()
        {
            return new MySqlConnection($"Server={_database.Server};Database={_database.Database};Uid={_database.User};Pwd={_database.Password};SslMode=Required;");
        }
    }
}
