using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using OpenFlights.Api.Models;

namespace OpenFlights.Api.Persistence
{
    public class AirlineRepository : IAirlineRepository
    {
        private readonly IConnectionFactory _connectionFactory;

        public AirlineRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory ?? throw new ArgumentNullException(nameof(connectionFactory));
        }

        public async Task<IReadOnlyList<Airline>> FetchAirportAirlines(uint id)
        {
            using (var connection = _connectionFactory.Create())
            {
                return (await connection.QueryAsync<Airline>(
                    "SELECT DISTINCT a.alid AS Id,a.name FROM airlines a INNER JOIN routes r ON a.alid=r.alid  WHERE r.src_apid=@id OR r.dst_apid=@id",
                    new {id})).ToArray();
            }
        }
    }
}
