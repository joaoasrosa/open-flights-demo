using System.Collections.Generic;
using System.Threading.Tasks;
using OpenFlights.Api.Models;

namespace OpenFlights.Api.Persistence
{
    public interface IAirportRepository
    {
        Task<IReadOnlyList<Airport>> FetchAirports();

        Task<AirportDetail> FetchAirport(uint id);
    }
}
