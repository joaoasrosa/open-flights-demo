using System.Collections.Generic;
using System.Linq;
using OpenFlights.Api.Models;

namespace OpenFlights.Api.Persistence
{
    internal class OpenWeatherResponse
    {
        public string Name { get; set; }

        public IEnumerable<WeatherDescription> Weather { get; set; }

        public Main Main { get; set; }

        internal Weather ToWeather()
        {
            return new Weather
            {
                Conditions = $"{Weather?.FirstOrDefault()?.Main} - {Weather?.FirstOrDefault()?.Description}",
                Temperature = float.Parse(Main.Temp)
            };
        }
    }

    public class WeatherDescription
    {
        public string Main { get; set; }
        public string Description { get; set; }
    }

    public class Main
    {
        public string Temp { get; set; }
    }
}
