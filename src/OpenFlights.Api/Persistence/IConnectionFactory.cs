using System.Data;

namespace OpenFlights.Api.Persistence
{
    public interface IConnectionFactory
    {
        IDbConnection Create();
    }
}
