﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OpenFlights.Crawler.Dtos;

namespace OpenFlights.Crawler
{
    class Program
    {
        private static IConfigurationRoot _configuration;
        private static readonly Statistics Statistics = new Statistics();

        static void Main(string[] args)
        {
            ConfigureApp();

            FetchData();
        }

        private static void ConfigureApp()
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json",
                    optional: false,
                    reloadOnChange: true)
                .AddUserSecrets<Program>();

            _configuration = builder.Build();
        }

        private static void FetchData()
        {
            var airports = FetchAirports();

            FetchAirport(airports);

            FetchAirportRoutes(airports);

            FetchAirportAirlines(airports);
        }

        private static IReadOnlyList<Airport> FetchAirports()
        {
            try
            {
                Statistics.StartCall();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_configuration["ApiUrl"]);
                    var response = client.GetAsync("/api/airports/").Result;
                    response.EnsureSuccessStatusCode();

                    Statistics.CallSucceed();

                    var stringResult = response.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<List<Airport>>(stringResult);
                }
            }
            catch
            {
                Statistics.CallFailed();
                return new Airport[0];
            }
        }

        private static void FetchAirport(IReadOnlyList<Airport> airports)
        {
            FetchAirportData(airports, string.Empty);
        }

        private static void FetchAirportRoutes(IReadOnlyList<Airport> airports)
        {
            FetchAirportData(airports, "routes");
        }

        private static void FetchAirportAirlines(IReadOnlyList<Airport> airports)
        {
            FetchAirportData(airports, "airlines");
        }

        private static void FetchAirportData(IReadOnlyList<Airport> airports, string endpoint)
        {
            foreach (var airport in airports)
            {
                try
                {
                    Statistics.StartCall();

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(_configuration["ApiUrl"]);
                        var response = client.GetAsync($"/api/airports/{airport.Id}/{endpoint}").Result;
                        response.EnsureSuccessStatusCode();

                        Statistics.CallSucceed();
                    }
                }
                catch
                {
                    Statistics.CallFailed();
                }
            }
        }
    }

    internal class Statistics
    {
        private readonly Stopwatch _stopwatch;
        private readonly List<long> _callsDuration;
        private uint _requests;
        private uint _failedRequests;

        internal Statistics()
        {
            _stopwatch = new Stopwatch();
            _callsDuration = new List<long>();
            _requests = 0;
            _failedRequests = 0;
        }

        internal void StartCall()
        {
            _requests++;

            _stopwatch.Reset();
            _stopwatch.Start();
        }

        internal void CallSucceed()
        {
            CollectCallDuration();

            UpdateStatistics();
        }

        internal void CallFailed()
        {
            CollectCallDuration();

            _failedRequests++;

            UpdateStatistics();
        }

        private void CollectCallDuration()
        {
            _stopwatch.Stop();
            _callsDuration.Add(_stopwatch.ElapsedMilliseconds);
        }

        private void UpdateStatistics()
        {
            Console.Write("\x000DHTTP requests: {0:D6} HTTP failed requests: {1:D6} HTTP avg calls duration: {2}",
                _requests,
                _failedRequests,
                _callsDuration.Average()
            );
        }
    }
}
